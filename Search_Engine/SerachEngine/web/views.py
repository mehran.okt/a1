import json

import requests
from django.shortcuts import render


# Create your views here.
def index(request):
    return render(request, "front/index.html")


def search(request):
    if request.method == "GET":
        var = []
        var2 = []
        mehran = []
        item = request.GET.get('item', None)
        ali = requests.get('http://localhost:9200/_search/?q=' + item)
        temp = json.loads(ali.text)
        temp = temp["hits"]
        for x in temp["hits"]:
            ali = requests.get(x["_source"]["data"]["seo"]["url"])
            j = "آگهی مورد نظر دیگر در سایت وجود ندارد، آگهی های مشابه در پایین آمده است."
            if ali.text.find(j) != -1:
                requests.delete('http://localhost:9200/divar/amlak/' + x['_id'])
                print('ok')
            else:
                var.append(x["_source"]["data"]["seo"])
        for val in var:
            if "thumbnail" in val:
                d = {
                    'title': val["title"],
                    'description': val["description"],
                    'uri': val["url"],
                    'thumbnail': val["thumbnail"],
                }
                var2.append(d)
            else:
                d = {
                    'title': val["title"],
                    'description': val["description"],
                    'uri': val["url"],
                    'thumbnail': 'static/images/test.jpg',
                }
                var2.append(d)
        # return JsonResponse(var2, safe=False)
        context = {
            'data': var2
        }
    return render(request, 'front/result.html', context)
